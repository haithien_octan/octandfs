# Octan Distribted System Source

Được viết dựa trên Google File System nhưng tối ưu hóa cho xử lý video.

Phát triển các node trên hệ điều hành Linux trước. 

Chương trình được viết bằng ngôn ngữ Python mô phỏng kiến trúc tổng thế của hệ thống lưu trữ phân tán. 

Cho phép clien có thể create, read, write, delete và restore files bên trong hệ thống lưu trữ phân tán trong khi cũng cho phép phục hồi dữ liệu của chunkserver.

## Master

Master manages the entire control flow of the system. It acts as an initiator of the communication between the client and the chunkservers. It handles the creation, removal and management of chunks and files and ensures the correct behaviour of the system.

``` python master.py <master_url> <master_port> ```

## Chunkserver

Chunkserver manages the storage of chunks. It handles read and write requests from the client.

``` python chunkserver.py <chunkserver_url> <chunkserver_port> <master_url> <master_port> <storage_dir> ```

## Client

Client will be the endpoint available for the end applications to perform operations on stored files in Half Baked GFS.

``` python client.py <client_url> <client_port> <master_url> <master_port> ```

## Test script for client

Test script simulates end application which makes use of client to perform operations over the files stored in Half Baked GFS.

Fill IP1, IP2, IP3 in IPS list which corresponds to ip addresses of the chunkservers.
``` python test_client.py <client_url> <client_port> ```

## Links
- [Report - Updating](https://gitlab.com/-/ide/project/haithien_octan/octandfs/tree/main/-/project_note/references/Report.pdf/)
- [Presentation - Updating](https://drive.google.com/file/d/10_SOlnbLlE6LVK6HJgH3xj9b5yZJcxM7/view?usp=sharing)
- [Slides - Updating](https://gitlab.com/-/ide/project/haithien_octan/octandfs/tree/main/-/project_note/references/Half-baked%20GFS.pptx/)
- [Slides - Stanford](https://cs.stanford.edu/~matei/courses/2015/6.S897/slides/gfs.pdf)

## References
- [The Google File System (SOSP, 2003)](https://static.googleusercontent.com/media/research.google.com/en//archive/gfs-sosp2003.pdf)
