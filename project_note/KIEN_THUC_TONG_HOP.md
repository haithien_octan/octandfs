## Distributed Video Storage?

<details>
<summary><b> Khái niệm:</b></summary>

Hệ thống tệp phân tán (DVS) là một hệ thống tệp cho phép bạn truy cập các video được lưu trữ trong nhiều máy chủ khác nhau.

Dữ liệu của DVS được lưu trữ ở dạng thô và dưới dạng tệp thay vì bảng hoặc định dạng tài liệu cố định như SQL hoặc NoSQL

DVS được tối ưu cho việc lưu trữ nội dung đa phương tiện như video, video360, hình ảnh, hình ảnh 360 dung lượng lớn
</details>

<details>
<summary><b> Các điểm chính cần xem xét khi thiết kế một DFS:</b></summary>

<b>Consistency</b> - Tính nhất quán cao đối với người dùng có nghĩa là hệ thống sẽ hoạt động như thể người dùng đang nói chuyện với một máy chủ duy nhất mặc dù thực tế đằng sau đang là 1000 máy chủ khác. Nếu người dùng chọn xây dựng một hệ thống phân tán cũng nhất quán về bản chất, hiệu suất ghi dữ liệu cần phải được quan tâm. Khi người dùng đưa 1 video lên, việc ghi dữ liệu được coi là thành công khi và chỉ khi các bản sao của video được ghi thành công.

<b>Parallel performance(through sharding and replication)</b>  Video có thể được chia nhỏ dữ liệu phân đoạn (shard) để lưu trữ trong nhiều máy để chia tỷ lệ với lượng dữ liệu cũng như tải lưu lượng tăng lên. Điều này chắc chắn sẽ làm tăng độ phức tạp của truy vấn và bảo trì khi lưu trữ dữ liệu, vì chúng ta cũng phải quản lý siêu dữ liệu ở lớp ứng dụng, nhưng đây là một cách tốt để lưu trữ nếu dữ liệu rất lớn

<b>Fault-tolerant (through replication)</b> - Hệ thống có tính khả dụng cao, tức là ngay cả khi một vài máy chủ bị lỗi, nó sẽ tiếp tục hoạt động mà không có bất kỳ thay đổi nào đối với người dùng cuối. Điều này có thể đạt được bằng cách nhân rộng, tức là sao chép và lưu trữ cùng một dữ liệu trên nhiều máy. Mặc dù việc nhân bản có thể rất khó để đạt được và chúng ta phải đánh đổi tính nhất quán để đạt được điều này, nhưng điều này chắc chắn có thể rất hữu ích nếu mất một tệp, thậm chí một phần nhỏ của nó có thể rất tốn kém cho doanh nghiệp. Ví dụ: nếu ai đó tin tưởng chúng ta để lưu giữ tất cả video của họ suốt đời, chúng ta không muốn họ mất bất kỳ dữ liệu nào và việc mất dù chỉ một chút dữ liệu cũng có thể gây hại cho danh tiếng của chúng ta.

<b>Automatic failure recovery</b> Tự động khắc phục lỗi, sẽ được thêm vào khi hệ thống triển khai thực tế

</details>

<details>
<summary><b>Kiến trúc hệ thống:</b></summary>

![dfsar](./imgdoc/dfsarchitecture.png) <b>Hình 1: Kiến trúc hệ thống </b>

<b>Metadata service(servers)</b> Metadata service(servers) chịu trách nhiệm theo dõi tất cả các metadata như nơi lưu trữ tệp, cách tệp được phân chia, cách sao chép, cập nhật pattern(versions). Chúng ta sẽ sử dụng kiến trúc master-slave nghĩa là sẽ có một nút duy nhất sẽ xử lý việc cập nhật tệp. Trong trường hợp nút này không hoạt động, các nút còn lại sẽ được chọn lựa để chọn ra một nút master mới trong số chúng. Sau đó video ở nút này sẽ được chia thành các bản sao lưu trữ ở nút khác


<b>Responsibilities of the master node:</b>  Master node, giữ thông tin liên quan đến việc lưu trữ (mapping) giữa video file name và chunks
Chúng ta sẽ truy vấn (query) master node để hỏi server rằng video đang được lưu trữ ở những đâu. Master luôn chứa 2 bảng chính:

<ol>
<li>Ánh xạ (map) file name thành một mảng chunk ids hoặc chunks handles</li>
<li>Ánh xạ chunk handle tới
<ul>
<li>Danh sách các máy chủ chunks servers.</li>
<li>Chunk server chính.</li>
</ul>
</li>
</ol>

Tất cả bảng dữ liệu nên được lưu trong in-memory db của master node để đọc và ghi nhanh hơn. Định kỳ đồng bộ hóa tất cả dữ liệu vào bộ nhớ không bay hơi (ổ cứng): Trong trường hợp bị lỗi, master node mới được bầu chọn sẽ tải những dữ liệu này từ ổ cứng.
	
<b>Responsibilities of slave node(s):</b> Giữ toàn bộ dữ liệu luôn đồng bộ với master. Trong trường hợp bị lỗi, bầu ra một master node mới

<b>Automatic failure recovery</b> Chịu trách nhiệm phân chia video và lưu trữ trên ổ cứng (không bay hơi). Các video được chia thành nhiều phần nhỏ hơn, vì video có thể rất lớn và để cập nhật, chúng tôi có thể không yêu cầu toàn bộ video được tải đi tải lại. Việc sao chép và đồng bộ hóa các video nhỏ trên các secondary servers và clients thứ cấp cũng sẽ dễ dàng hơn nếu nhiều clients được kết nối cùng một lúc.

<b>How reads and writes happen?</b>

Cả metadata service và chunking service đều tham gia trong bất cứ hoạt động reads hoặc write.

Read:
<ol>
<li>API: read(file_id, offset)
<ul>
<li>file_id: là thuộc tính để có thể xác định file</li>
<li>offset: sẽ là điểm bắt đầu trong toàn bộ video, từ đó nó sẽ bắt đầu đọc</li>
</ul>
</li>
<li>Yêu cầu read từ client sẽ được đưa đến metadata service đầu tiên, dịch vụ này sẽ trả về:
<ul>
<li>dịch vụ phân đoạn cho tệp và offset.</li>
<li>danh sách chunks server, bao gồm cả server chính (primary server) và server thứ cấp (secondary – nơi lưu trữ bản sao).</li>
</ul>
</li>
<li>Thông tin trên cũng sẽ được lưu vào bộ nhớ cache, vì cuộc gọi tương tự sẽ rất thường xuyên. Sau đó, yêu cầu Read sẽ chuyển đến máy chủ chunk dưới dạng read (chunk_handle, offset) nơi 	chunk_handle sẽ có thông tin về phiên bản chunks. Điều này sẽ trả về phân đoạn(chunk) của tệp từ một trong các secondary chunk servers</li>
</ol>

Write:
<ol>
<li>API: write(data, file_id)
<ul>
<li>data: được thêm vào cuối tệp. </li>
<li>file_id : là thuộc tính để có thể xác định file</li>
</ul>
</li>
<li>Yêu cầu sẽ gửi đến metadata service, nơi sẽ tạo ra các mã định danh phân đoạn (chunk) cho new data:
<ul>
<li>Lưu trữ chunk handles, version.</li>
<li>Tìm nơi sao chép dữ liệu (chunk servers).</li>
</ul>
</li>
<li>Bầu một trong số làm chính, những cái khác là phụ. Master của máy chủ siêu dữ liệu sẽ tăng số phiên bản và ghi nó vào đĩa</li>
</ol>

<b>Điều gì xảy ra nếu metaservices nghĩ rằng chunk server chính bị chết? </b> 
Điều này xảy ra trong nhiều hoàn cảnh như là network failure, packet loss, etc Khi đó metadata service nghĩ rằng chunk server chính bị chết, do đó sẽ bầu ra một primary chính và cập nhật vào database

Nhưng có những trường hợp, primary server vẫn kết nối phản hồi tới các secondary tốt, nhưng lại bị ngắt kết nối đến metadata service. Trong trường hợp này, chúng ta sẽ có 2 primary servers, đây là một vấn đề lớn.

Trong trường hợp này chúng ta gọi là vấn đề “split-brain” Để giải quyết vấn đề này, chúng ta có thể dùng term TTL tới primary, ví dụ bao nhiêu lâu sẽ là primary, và sẽ ko nhận bất cứ requests(yêu cầu) nào sau khi TTL kết thúc. Sau khi TTL kết thúc, nó sẽ check xem các available nodes và chọn một trong số chúng làm node chính, giữ các node còn lại làm nốt thứ cấp và update chúng vào database.

</details>

## Replication Explained?

<details>

<summary><b> Tổng quan:</b></summary>

Replication in distributed system tham gia vào việc xử lý liên quan, chúng ta có nhiều bản copies của video tại nhiều vị trí để có thể tránh những lỗi phát sinh từ phần cứng.
</details>

<details>
<summary><b> Replication Architecture:</b></summary>
Có nhiều cách để sao chép video, ở đây octan tập trung đưa ra một kiến trúc phù hợp nhất. 
State Transfer (Passive Replication). Một chiều chúng ta có thể giữ primary và sao chép thông qua sync, chúng ta có thể cấu hình primary server để gửi một bản copy tới toàn bộ các state. Bản sao server sẽ phục hồi toàn bộ trạng thái dựa vào sync server khi primary server bị lỗi.
</details>
continue update

## POSIX (Portable Operating System Interface)

<details>
<summary><b> Tổng Quan:</b></summary>
Chuẩn POSIX định nghĩa API (Application Programming Interface), cùng với commandline shells và những giao diện hữu ích (utility interfaces) khác.
Để dễ hiểu, mình lấy ví dụ là shell trên HĐH GNU/Linux đều có các lệnh giống trên Unix (từ tên lệnh đến options, vẫn có khác biệt nhưng rất ít) như: ls, cat, pwd, cp, mv,…

GNU/Linux là hệ điều hành chuẩn POSIX

Một cách dễ hiểu thì Octan DFS sẽ phát triển hạ tầng dựa trên kiến thức hệ điều hành Linux/Ubuntu. 
</details>


## MapReduce

<details>
<summary><b> Tổng Quan:</b></summary>
Updating
</details>

## (Cơ sở dữ liệu trên RAM) In-memory database

<details>
<summary><b> Tổng Quan:</b></summary>
Updating
</details>



